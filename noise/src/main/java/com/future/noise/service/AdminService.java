package com.future.noise.service;

import com.future.noise.model.Admin;

public interface AdminService {

    void register(Admin admin);

    Admin login(String adminName,String password);
}
