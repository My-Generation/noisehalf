package com.future.noise.service;

import com.future.noise.model.ComplainRecord;

import java.util.List;

public interface ComplainRecordService {
    //查询所有
    List<ComplainRecord> findAll();
    //添加记录
    void add(ComplainRecord complainRecord);
    //删除记录
    void delete(String id);
    //更新信息
    void update(ComplainRecord complainRecord);
    //根据id查询唯一记录
    ComplainRecord find(String id);
    //根据被投诉楼号查询
    List<ComplainRecord> findByBeComBuildingNumber(int beComBuildingNumber);
    //根据楼号和房间号查询
    List<ComplainRecord> findByBeComBuildingNumberAndHouseNumber(int beComBuildingNumber, int beComHouseNumber);
    //根据内容关键字查询
    List<ComplainRecord> findByKeyWord(String keyWord);
    //根据用户自身楼号和房间号查询历史
    List<ComplainRecord> findOwn(int comBuildingNumber,int comHouseNumber);
}
