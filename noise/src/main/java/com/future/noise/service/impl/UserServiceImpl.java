package com.future.noise.service.impl;

import com.future.noise.dao.UserDAO;
import com.future.noise.model.User;
import com.future.noise.service.UserService;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public void register(User user){
        userDAO.save(user);
    }

    @Override
    public User login(String username,String password){
        return userDAO.queryByUsernameAndPassword(username,password);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<User> findAll(){
        return userDAO.findAll();
    }

    @Override
    public void delete(String id){
        userDAO.delete(id);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public User find(String id){
        return userDAO.find(id);
    }

    @Override
    public int update(User user){
        return userDAO.update(user);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<User> findByMobile(String mobile){
        return userDAO.findByMobile(mobile);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<User> findUserByKeyWord(String keyWord){
        return userDAO.findByKeyWord(keyWord);
    }


}
