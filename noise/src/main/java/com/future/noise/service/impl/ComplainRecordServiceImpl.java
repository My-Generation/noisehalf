package com.future.noise.service.impl;

import com.future.noise.dao.ComplainRecordDAO;
import com.future.noise.model.ComplainRecord;
import com.future.noise.service.ComplainRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ComplainRecordServiceImpl implements ComplainRecordService {

    @Autowired
    private ComplainRecordDAO complainRecordDAO;

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ComplainRecord> findAll(){
        return complainRecordDAO.findAll();
    }

    @Override
    public void add(ComplainRecord complainRecord){
        complainRecordDAO.save(complainRecord);
    }

    @Override
    public void delete(String id){
        complainRecordDAO.delete(id);
    }

    @Override
    public void update(ComplainRecord complainRecord){
        complainRecordDAO.update(complainRecord);
    }

    @Override
    public ComplainRecord find(String id){
        return complainRecordDAO.find(id);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ComplainRecord> findByBeComBuildingNumber(int beComBuildingNumber){
        return complainRecordDAO.findByBeComBuildingNumber(beComBuildingNumber);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ComplainRecord> findByBeComBuildingNumberAndHouseNumber(int beComBuildingNumber, int beComHouseNumber){
        return complainRecordDAO.findByBeComBuildingNumberAndHouseNumber(beComBuildingNumber,beComHouseNumber);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ComplainRecord> findByKeyWord(String keyWord){
        return complainRecordDAO.findByKeyWord(keyWord);
    }

    @Override
    @Transactional(propagation = Propagation.SUPPORTS)
    public List<ComplainRecord> findOwn(int comBuildingNumber, int comHouseNumber){
        return complainRecordDAO.findOwn(comBuildingNumber,comHouseNumber);
    }

}
