package com.future.noise.service;

import com.future.noise.model.User;

import java.util.List;

public interface UserService {
    //注册用户
    void register(User user);
    //用户登录验证查询
    User login(String username,String password);
    //查询所有用户
    List<User> findAll();
    //删除用户
    void delete(String id);
    //根据id查询唯一用户
    User find(String id);
    //更改用户信息
    int update(User user);
    //根据手机号查询
    List<User> findByMobile(String mobile);
    //根据关键词查询
    List<User> findUserByKeyWord(String keyWord);
}
