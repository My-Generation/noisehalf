package com.future.noise.controller;

import com.future.noise.model.Admin;
import com.future.noise.model.User;
import com.future.noise.service.AdminService;
import com.future.noise.service.UserService;
import com.future.noise.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userService;

    //登录方法
    @PostMapping("/login")
    public String login(String adminName, String password, HttpSession session){
        Admin loginAdmin = adminService.login(adminName,password);
        if(loginAdmin!=null){
            session.setAttribute("admin",loginAdmin);
            return "redirect:/adminPage";
        }else{
            return "redirect:/adminLogin";
        }
    }

    //注册方法
    @PostMapping("/register")
    public String register(Admin admin){
        admin.setId(UUIDUtils.getUUID());
        adminService.register(admin);
        return "redirect:/adminLogin";
    }
    /*

     */

    //管理员根据手机查找所有有关用户
    @PostMapping("/findUserByMobile")
    public String findUserByMobile(String mobile,Model model){
        List<User> users = userService.findByMobile(mobile);
        if(users==null){
            User u = new User();
            u.setUsername("空");
            users.add(u);
        }
        model.addAttribute("users",users);
        return "/web/findUserModel";
    }

    //管理员删除用户
    @GetMapping("/delete")
    public String delete(String id){
        userService.delete(id);
        return "redirect:/inputMobile";
    }

    //根据id查询唯一用户
    @GetMapping("/find")
    public String find(String id, Model model){
        User user = userService.find(id);
        model.addAttribute("user",user);
        return "/web/adminMobileUpdate";
    }

    //管理员更新用户方法
    @PostMapping("/update")
    public String updateUser(User user){
        int result = userService.update(user);
        return "redirect:/inputMobile";
    }
    /*

     */

    //管理员根据用户名关键词查询用户
    @PostMapping("/findUserByKeyWord")
    public String findUserByKeyWord(String keyWord,Model model){
        List<User> users = userService.findUserByKeyWord(keyWord);
        if(users==null){
            User u = new User();
            u.setUsername("空");
            users.add(u);
        }
        model.addAttribute("users",users);
        return "/web/findUserKeyWordModel";
    }

    //管理员删除用户
    @GetMapping("/deleteKey")
    public String deleteKey(String id){
        userService.delete(id);
        return "redirect:/inputUserKeyWord";
    }

    //根据id查询唯一用户
    @GetMapping("/findKey")
    public String findKey(String id, Model model){
        User user = userService.find(id);
        model.addAttribute("user",user);
        return "/web/adminKeyWordUpdate";
    }

    //管理员更新用户方法
    @PostMapping("/updateKey")
    public String updateUserKey(User user){
        int result = userService.update(user);
        return "redirect:/inputUserKeyWord";
    }

}
