package com.future.noise.controller;

import com.future.noise.model.ComplainRecord;
import com.future.noise.model.ComplainRecordDetail;
import com.future.noise.service.ComplainRecordService;
import com.future.noise.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/complainRecord")
public class ComplainRecordController {
    @Autowired
    private ComplainRecordService complainRecordService;

    //查询所有记录
    @GetMapping("/findAllComplainRecord")
    public String toFindAllComplainRecord(Model model){
        List<ComplainRecord> complainRecords = complainRecordService.findAll();
        model.addAttribute("complainRecords",complainRecords);
        return "web/findAllComplainRecord";
    }

    //添加一条记录
    @PostMapping("/addComplainRecord")
    public String addComplainRecord(ComplainRecord complainRecord){
        complainRecord.setId(UUIDUtils.getUUID());
        complainRecord.setSolve("否");
        complainRecordService.add(complainRecord);
        return "redirect:/userPage";
    }
    //删除投诉记录
    @GetMapping("/delete")
    public String delete(String id){
        complainRecordService.delete(id);
        return "redirect:/complainRecord/findAllComplainRecord";
    }
    //根据id查询唯一投诉记录
    @GetMapping("/find")
    public String find(String id, Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/updateComplainRecord";
    }

    //查询详细(All)
    @GetMapping("/findDetail")
    public String findDetail(String id, Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/ComplainRecordDetail";
    }

    //更新投诉
    @PostMapping("/update")
    public String update(ComplainRecord complainRecord){
        complainRecordService.update(complainRecord);
        return "redirect:/complainRecord/findAllComplainRecord";
    }

    /*
     *
     */

    //根据被投诉楼号查询投诉
    @PostMapping("/findComRecordByBeComBuildingNumber")
    public String findComByBeComBuildingNumber(int beComBuildingNumber,Model model){
         List<ComplainRecord> complainRecords = complainRecordService.findByBeComBuildingNumber(beComBuildingNumber);
         model.addAttribute("complainRecords",complainRecords);
         return "/web/findComByBeComBuilding";
    }
    //被投诉楼号删除
    @GetMapping("/deleteBeComBuildingNumber")
    public String deleteBeComBuildingNumber(String id){
        complainRecordService.delete(id);
        return "redirect:/inputBeComBuildingNumber";
    }

    //被投诉根据id查询唯一
    @GetMapping("/findBeComBuildingNumber")
    public String findBeComBuildingNumber(String id,Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/updateComplainRecordByBeComBuildingNumber";
    }

    //被投诉更新投诉
    @PostMapping("/updateBeComBuildingNumber")
    public String updateBeComBuildingNumber(ComplainRecord complainRecord){
        complainRecordService.update(complainRecord);
        return "redirect:/inputBeComBuildingNumber";
    }

    /*
     *
     */
    //根据被投诉楼号查询投诉
    @PostMapping("/findComRecordByBeComBuildingNumberAndHouseNumber")
    public String findComByBeComBuildingNumberAndHouseNumber(int beComBuildingNumber,int beComHouseNumber,Model model){
        List<ComplainRecord> complainRecords =
                complainRecordService.findByBeComBuildingNumberAndHouseNumber(beComBuildingNumber,beComHouseNumber);
        model.addAttribute("complainRecords",complainRecords);
        return "/web/findComByBeComBuildingAndHouse";
    }

    //被投诉楼号房号删除
    @GetMapping("/deleteBeComBuildingNumberAndHouseNumber")
    public String deleteBeComBuildingNumberAndHouseNumber(String id){
        complainRecordService.delete(id);
        return "redirect:/inputBeComBuildingNumberAndHouseNumber";
    }

    //被投诉根据id查询唯一
    @GetMapping("/findBeComBuildingNumberAndHouseNumber")
    public String findBeComBuildingNumberAndHouseNumber(String id,Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/updateComByBuildingAndHouse";
    }

    //被投诉更新投诉
    @PostMapping("/updateBeComBuildingNumberAndHouseNumber")
    public String updateBeComBuildingNumberAndHouseNumber(ComplainRecord complainRecord){
        complainRecordService.update(complainRecord);
        return "redirect:/inputBeComBuildingNumberAndHouseNumber";
    }
    /*

     */
    //根据关键词查询
    @PostMapping("/findComRecordByKeyWord")
    public String findComRecordByKeyWord(String keyWord,Model model){
        List<ComplainRecord> complainRecords = complainRecordService.findByKeyWord(keyWord);
        model.addAttribute("complainRecords",complainRecords);
        return "/web/findComByKeyWord";
    }
    //关键词删除
    @GetMapping("/deleteKey")
    public String deleteKey(String id){
        complainRecordService.delete(id);
        return "redirect:/inputComKeyWord";
    }
    //关键词唯一id查询
    @GetMapping("/findKey")
    public String findKey(String id,Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/updateComByKeyWord";
    }

    @PostMapping("/updateKey")
    public String updateKey(ComplainRecord complainRecord){
        complainRecordService.update(complainRecord);
        return "redirect:/inputComKeyWord";
    }
    /*

     */

    @PostMapping("/findOwnHistory")
    public String findOwnHistory(int comBuildingNumber,int comHouseNumber,Model model){
        List<ComplainRecord> complainRecords =
                complainRecordService.findOwn(comBuildingNumber,comHouseNumber);
        model.addAttribute("complainRecords",complainRecords);
        return "/web/findOwn";
    }

    @GetMapping("/deleteOwn")
    public String deleteOwn(String id){
        complainRecordService.delete(id);
        return "redirect:/inputBuildingAndHouse";
    }

    @GetMapping("/findOwn")
    public String findOwn(String id,Model model){
        ComplainRecord complainRecord = complainRecordService.find(id);
        model.addAttribute("complainRecord",complainRecord);
        return "/web/updateComByUser";
    }

    @PostMapping("/updateOwn")
    public String updateOwn(ComplainRecord complainRecord){
        complainRecordService.update(complainRecord);
        return "redirect:/inputBuildingAndHouse";
    }
}
