package com.future.noise.controller;

import com.future.noise.dao.UserDAO;
import com.future.noise.model.ComplainRecord;
import com.future.noise.model.User;
import com.future.noise.service.ComplainRecordService;
import com.future.noise.service.UserService;
import com.future.noise.utils.UUIDUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    //登录方法
    @PostMapping("/login")
    public String login(String username, String password, HttpSession session,Model model){
        User loginUser = userService.login(username,password);
        if(loginUser!=null){
            session.setAttribute("user",loginUser);
            return "redirect:/userPage";
        }else{
            String message = "1";
            model.addAttribute("message",message);
            return "redirect:/toRegister";
        }
    }

    //注册方法
    @PostMapping("/register")
    public String register(User user){
        user.setId(UUIDUtils.getUUID());
        userService.register(user);
        //跳转到登录界面
        return "redirect:/loginPage";
    }

    //查询所有用户方法
    @GetMapping("/findAllUser")
    public String findAll(Model model){
        List<User> users = userService.findAll();
        model.addAttribute("users",users);
        return "web/findAllUser";
    }

    //管理员添加用户方法
    @PostMapping("/addUser")
    public String addUser(User user){
        user.setId(UUIDUtils.getUUID());
        userService.register(user);
        return "redirect:/user/findAllUser";
    }
    //管理员删除用户
    @GetMapping("/delete")
    public String delete(String id){
        userService.delete(id);
        return "redirect:/user/findAllUser";
    }

    //根据id查询唯一用户
    @GetMapping("/find")
    public String find(String id, Model model){
        User user = userService.find(id);
        model.addAttribute("user",user);
        return "/web/updateUser";
    }

    //管理员更新用户方法
    @PostMapping("/update")
    public String updateUser(User user){
        int result = userService.update(user);
        return "redirect:/user/findAllUser";
    }

    /*

     */
}
