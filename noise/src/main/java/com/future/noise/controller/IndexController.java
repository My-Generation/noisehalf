package com.future.noise.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping("/loginPage")
    public String toIndex(){
        return "web/login";
    }

    @GetMapping("/toRegister")
    public String toRegister(){
        return "web/register";
    }

    @GetMapping("/userPage")
    public String toUserPage(){
        return "web/userPage";
    }

    @GetMapping("/adminLogin")
    public String toAdminLogin(){
        return "web/adminLogin";
    }

    @GetMapping("/adminRegister")
    public String toAdminRegister(){
        return "web/adminRegister";
    }

    @GetMapping("/adminPage")
    public String toAdminPage(){
        return "/web/adminPage";
    }

    @GetMapping("/saveUser")
    public String toSaveUser(){
        return "web/addUser";
    }

    @GetMapping("/findAllUserPage")
    public String toFindAllUser(){
        return "redirect:/user/findAllUser";
    }

    @GetMapping("/addComplainRecord")
    public String toAddComplainRecord(){
        return "web/addComplainRecord";
    }

    @GetMapping("/inputMobile")
    public String inputMobile(){
        return "/web/inputMobile";
    }

    @GetMapping("/inputUserKeyWord")
    public String inputUserKeyWord(){
        return "/web/inputUserKeyWord";
    }

    @GetMapping("/inputBeComBuildingNumber")
    public String inputBeComBuildingNumber(){
        return "/web/inputBeComBuildingNumber";
    }

    @GetMapping("/inputBeComBuildingNumberAndHouseNumber")
    public String inputBeComBuildingNumberAndHouseNumber(){
        return "/web/inputBeComBuildingNumberAndHouseNumber";
    }

    @GetMapping("/inputComKeyWord")
    public String inputComKeyWord(){
        return "/web/inputComKeyWord";
    }

    @GetMapping("/inputBuildingAndHouse")
    public String inputBuildingAndHouse(){
        return "/web/inputBuildingAndHouse";
    }

    @GetMapping("/contact")
    public String contact(){
        return "/web/contact";
    }
}
