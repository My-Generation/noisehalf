package com.future.noise.dao;

import com.future.noise.model.ComplainRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ComplainRecordDAO {
    //查询所有投诉记录
    List<ComplainRecord> findAll();
    //添加投诉记录
    void save(ComplainRecord complainRecord);
    //删除记录
    void delete(String id);
    //更新信息
    void update(ComplainRecord complainRecord);
    //根据id查询唯一的投诉
    ComplainRecord find(String id);
    //根据楼号查询
    List<ComplainRecord> findByBeComBuildingNumber(int beComBuildingNumber);
    //根据被投诉楼号和房间号查询
    List<ComplainRecord> findByBeComBuildingNumberAndHouseNumber
    (@Param("beComBuildingNumber")int beComBuildingNumber,@Param("beComHouseNumber")int beComHouseNumber);
    //投诉内容关键词查询
    List<ComplainRecord> findByKeyWord(String keyWord);
    //投诉者查询历史投诉
    List<ComplainRecord> findOwn(@Param("comBuildingNumber")int comBuildingNumber,@Param("comHouseNumber")int comHouseNumber);
}
