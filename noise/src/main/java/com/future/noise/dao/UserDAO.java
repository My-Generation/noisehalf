package com.future.noise.dao;

import com.future.noise.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserDAO {
    //保存
    void save(User user);
    //根据用户
    User queryByUsernameAndPassword(@Param("username")String username,@Param("password")String password);
    //查询所有用户
    List<User> findAll();
    //删除用户
    void delete(String id);
    //根据ID查询唯一用户
    User find(String id);
    //修改用户信息
    int update(User user);
    //根据绑定手机号查询用户
    List<User> findByMobile(String mobile);
    //根据用户名关键词查找用户
    List<User> findByKeyWord(String keyWord);
}
