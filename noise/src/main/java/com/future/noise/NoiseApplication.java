package com.future.noise;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.future.noise","com.future.noise.service"})
@MapperScan("com.future.noise.dao")
public class NoiseApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoiseApplication.class, args);
	}

}
