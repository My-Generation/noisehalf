package com.future.noise.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public class ComplainRecordDetail {
    private String id;
    //投诉楼号
    private int comBuildingNumber;
    //投诉房号
    private int comHouseNumber;
    //投诉人联系方式
    private String comContact;
    //投诉内容
    private String content;
    //被投诉楼号
    private int beComBuildingNumber;
    //被投诉房号
    private int beComHouseNumber;
    //被投诉人联系方式
    private String beComContact;
    //是否解决
    private String solve;
    //创建日期
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtCreated;
    //修改日期
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private Date gmtModified;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setComBuildingNumber(int comBuildingNumber) {
        this.comBuildingNumber = comBuildingNumber;
    }

    public int getComBuildingNumber() {
        return comBuildingNumber;
    }

    public void setComHouseNumber(int comHouseNumber) {
        this.comHouseNumber = comHouseNumber;
    }

    public int getComHouseNumber() {
        return comHouseNumber;
    }

    public void setComContact(String comContact){
        this.comContact = comContact;
    }

    public String getComContact() {
        return comContact;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setBeComBuildingNumber(int beComBuildingNumber) {
        this.beComBuildingNumber = beComBuildingNumber;
    }

    public int getBeComBuildingNumber() {
        return beComBuildingNumber;
    }

    public void setBeComHouseNumber(int beComHouseNumber) {
        this.beComHouseNumber = beComHouseNumber;
    }

    public int getBeComHouseNumber() {
        return beComHouseNumber;
    }

    public void setBeComContact(String beComContact) {
        this.beComContact = beComContact;
    }

    public String getBeComContact() {
        return beComContact;
    }

    public void setSolve(String solve) {
        this.solve = solve;
    }

    public String getSolve() {
        return solve;
    }

    public void setGmtCreated(Date gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Date getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtModified(Date gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Date getGmtModified() {
        return gmtModified;
    }
}
